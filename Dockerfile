FROM ubuntu:14.04

#Install nodejs
RUN apt-key update && apt-get update && apt-get install -y nodejs npm
RUN update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app

RUN ls -l

EXPOSE 8080
CMD [ "npm", "start" ]